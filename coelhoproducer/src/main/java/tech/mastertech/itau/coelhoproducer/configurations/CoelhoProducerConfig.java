package tech.mastertech.itau.coelhoproducer.configurations;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CoelhoProducerConfig {

		@Bean
		public FanoutExchange fanout() {
			return new FanoutExchange("pubsubber");
		}
	
}
