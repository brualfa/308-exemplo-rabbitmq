package tech.mastertech.itau.coelhoproducer.controllers;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cenoura")
public class CenouraController {
	
	@Autowired
	private RabbitTemplate template;
	
	@Autowired
	private FanoutExchange exchange;
	
	@PostMapping
	public void criarCenoura(@RequestParam String descricao) {
		template.convertAndSend(exchange.getName(), "", descricao);
	}
}
