package tech.mastertech.itau.coelhoproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoelhoproducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoelhoproducerApplication.class, args);
	}

}
