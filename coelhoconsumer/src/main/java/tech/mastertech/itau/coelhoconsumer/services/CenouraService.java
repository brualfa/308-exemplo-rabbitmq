package tech.mastertech.itau.coelhoconsumer.services;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "#{temporaria.name}")
public class CenouraService {
	
	@RabbitHandler
	public void comerCenoura(String descricao) {
		System.out.println("Cenoura " + descricao + " devorada vorazmente pelo consumer 1!");
	}
}
