package tech.mastertech.itau.coelhoconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoelhoconsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoelhoconsumerApplication.class, args);
	}

}
